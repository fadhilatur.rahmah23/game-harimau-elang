<?php

  require_once('class/init.php');

  $harimau1 = new Harimau('Harimau Sumatra');
  $elang1 = new Elang('Elang Satu');

  echo $harimau1->atraksi() . '<br>';
  echo $elang1->atraksi() . '<br>';
  echo '<br>';

  echo $elang1->serang($harimau1) . '<br>';
  echo $harimau1->diserang($elang1) . '<br>';
  echo '<br>';

  echo $harimau1->serang($elang1) . '<br>';
  echo $elang1->diserang($harimau1) . '<br>';
  echo '<br>';

  $harimau1->getInfo();
  echo '<br>';
  $elang1->getInfo();

?>
