<?php

  abstract class Hewan {
    private $nama,
            $darah = 50,
            $jumlahKaki,
            $keahlian;

    function __construct($nama, $jumlahKaki, $keahlian) {
      $this->nama = $nama;
      $this->jumlahKaki = $jumlahKaki;
      $this->keahlian = $keahlian;
    }

    public function getNama() {
      return $this->nama;
    }

    public function getDarah() {
      return $this->darah;
    }

    public function getJumlahKaki() {
      return $this->jumlahKaki;
    }

    public function getKeahlian() {
      return $this->keahlian;
    }

    public function setDarah($darah) {
      $this->darah = $darah;
    }

    public function atraksi() {
      $str = $this->nama . ' sedang ' . $this->keahlian;
      return $str;
    }

    public function getInfoHewan() {
      echo 'Nama: ' . $this->getNama() . '<br>';
      echo 'Jumlah Kaki: ' . $this->getJumlahKaki() . '<br>';
      echo 'Keahlian: ' . $this->getKeahlian() . '<br>';
      echo 'Jumlah Darah: ' . $this->getDarah() . '<br>';
    }
  }

?>
