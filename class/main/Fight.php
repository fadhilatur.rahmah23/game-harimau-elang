<?php

  trait Fight {
    private $attackPower,
            $defencePower;

    public function setAttackPower($attackPower) {
      $this->attackPower = $attackPower;
    }

    public function getAttackPower() {
      return $this->attackPower;
    }

    public function setDefencePower($defencePower) {
      $this->defencePower = $defencePower;
    }

    public function getDefencePower() {
      return $this->defencePower;
    }

    public function serang($hewan) {
      $str = $this->getNama() . ' sedang menyerang ' . $hewan->getNama();
      return $str;
    }

    public function diserang($hewanPenyerang) {
      $str = $this->getNama() . ' sedang diserang';
      $blood = $this->getDarah()-($hewanPenyerang->getAttackPower()/$this->getDefencePower());
      $this->setDarah($blood);
      return $str;
    }

    public function getInfoFight() {
      echo 'Attack Power: ' . $this->getAttackPower() . '<br>';
      echo 'Defence Power: ' . $this->getDefencePower() . '<br>';
    }

  }

?>
