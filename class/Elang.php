<?php

  require_once('main/init.php');

  class Elang extends Hewan {
    use Fight;

    function __construct($nama, $jumlahKaki = 2, $keahlian = 'terbang tinggi', $attackPower = 10, $defencePower = 5) {
      parent::__construct($nama, $jumlahKaki, $keahlian);
      $this->setAttackPower($attackPower);
      $this->setDefencePower($defencePower);
    }

    public function getInfo() {
      echo '=== INFORMASI HEWAN === <br>';
      echo 'Jenis Hewan: Elang <br>';
      parent::getInfoHewan();
      $this->getInfoFight();
    }
  }

?>
