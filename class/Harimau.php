<?php

  require_once('main/init.php');

  class Harimau extends Hewan {
    use Fight;

    function __construct($nama, $jumlahKaki = 4, $keahlian = "lari cepat", $attackPower = 7, $defencePower = 8) {
      parent::__construct($nama, $jumlahKaki, $keahlian);
      $this->setAttackPower($attackPower);
      $this->setDefencePower($defencePower);
    }

    public function getInfo() {
      echo '=== INFORMASI HEWAN === <br>';
      echo 'Jenis Hewan: Harimau <br>';
      parent::getInfoHewan();
      $this->getInfoFight();
    }
  }

?>
